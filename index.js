//Soal no 1
//buatlah fungsi menggunakan arrow function luas dan keliling persegi
//panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

//jawaban no 1
const luasPersegiPanjang = (panjang, lebar) => {
  return panjang * lebar;
};

const kelilingPersegiPanjang = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};

console.log(luasPersegiPanjang(2, 4));
console.log(kelilingPersegiPanjang(2, 4));

//soal no 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }

// //Driver Code
// newFunction("William", "Imoh").fullName()

//jawaban no 2
const newFunction = (firstName, lastName) => {
  const fullName = firstName + " " + lastName;
  return { fullName };
};

let p = newFunction("William", "Imoh");
console.log(p.fullName);

//soal no 3
//Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

//jawaban no 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

//soal no 4
//Kombinasikan dua array berikut menggunakan array spreading ES6

//jawaban no 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = [...west, "Gill", "Brian", "Noel", "Maggie"];
console.log(east);

//soal no 5
//sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

//jawaban no 5
const planet = "earth";
const view = "glass";
console.log(`Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet}`);
